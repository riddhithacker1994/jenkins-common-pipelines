import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
def generateSteps(job, builddata, cl) {
    return builddata.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}
def prebuild(Object buildconfig) {
    return {
        buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "beforeBuild", Object)) {
               it.proj.beforeBuild(it)
           }
       }
    } // end return
}

def code_quality(Object buildconfig) {
        return {
        container('maven-java8') {
            dir("${buildconfig.workingdir}") {
                sh """
                    echo "Nothing To Do"
                """
            }
        }
    } // end return
}

def unittests(Object buildconfig) {
    return {
       buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "beforeTest", Object)) {
               it.proj.beforeTest(it)
           }
       //}

       if ( it.lang.toLowerCase() == "xml" ) {
          container('postman') {
            dir("${buildconfig.workingdir}") {
                sh """
                npm install -g apigeelint
                cd "${buildconfig.workingdir}/${it.pomDir}"
                apigeelint -s apiproxy/ -f html.js > result.html  
                ls
                """
            }
            step([$class: 'ArtifactArchiver', artifacts: '**/*.html'])
            def exists = fileExists '${buildconfig.workingdir}/${it.pomDir}/result.html'
            if (exists) {
                echo 'html report file is generated'
                publishHTML (target: [
                allowMissing: false,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: '${it.pomDir}',
                reportFiles: 'result.html',
                reportName: "Coverage Report ${it.name}" ])
            }
          }   
       }
       //buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "afterTest", Object)) {
               it.proj.afterTest(it)
           }
       }
    } // end return
}
def build(Object buildconfig) {
    return {
        StandardUsernamePasswordCredentials artifactoryCreds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def artifactoryUser = artifactoryCreds.getUsername() 
        def artifactoryPass = Secret.toString(artifactoryCreds.getPassword())

        buildconfig.jobs.each {
          container('maven-java8') {
              dir("${buildconfig.workingdir}") {
                  writeFile file: 'mule-maven-settings.xml', text: libraryResource('mule-maven-settings.xml')
                  sh """
                    cd "${buildconfig.workingdir}/${it.pomDir}"
                    mvn -DskipTests clean package
                 """
              }
          }

      }

    } // end return
}

def postbuild(Object buildconfig) {
    return {
        buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "afterBuild", Object)) {
               it.proj.beforeBuild(it)
           }
       }
    } // end return
}

def publish(Object buildconfig) {
   return {
      buildconfig.jobs.each {
       if (it.proj.metaClass.respondsTo(it.proj, "beforePush", Object)) {
           it.proj.beforePush(it)
       }
       container('devopstoolsimage') {
               withCredentials([[

                                $class          : 'UsernamePasswordMultiBinding',
                                credentialsId   : "ARTIFACTORY_ACCESS",
                                usernameVariable: 'ARTIFACTORY_USERID',
                                passwordVariable: 'ARTIFACTORY_PASSWORD'
                            ]]) {

               dir("${buildconfig.workingdir}/${it.pomDir}") {
                   sh """
                       cd target/apiproxy
                       mv *.zip ${it.name}.zip
                       cd ..
                   """

                   sh """
                       curl --fail -u ${ARTIFACTORY_USERID}:${ARTIFACTORY_PASSWORD} \
                           --header "X-Checksum-MD5:\$(md5sum target/apiproxy/${it.name}.zip |awk '{print \$1}')" \
                           --header "X-Checksum-Sha1:\$(sha1sum target/apiproxy/${it.name}.zip |awk '{print \$1}')" \
                           -X PUT \"http://${it.arttifactserver}/artifactory/libs-snapshot-local/${it.repoName}/${it.branch}/${it.version}/${it.name}.zip\" \
                           -T "target/apiproxy/${it.name}.zip"
                       """
                   buildconfig["build_artifact_${it.name}"] = "http://${it.arttifactserver}/artifactory/libs-snapshot-local/${it.repoName}/${it.branch}/${it.version}/${it.name}.zip"
               }
           }    
       }
       if (it.proj.metaClass.respondsTo(it.proj, "afterPush", Object)) {
           it.proj.afterPush(it)
       }
    }
   } // end return
}

return this