import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
def generateSteps(job, builddata, cl) {
    return builddata.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}
def prebuild(Object buildconfig) {
    return {
        buildconfig.proj.beforeBuild(buildconfig)
    } // end return
}
def code_quality(Object buildconfig) {
        return {
        StandardUsernamePasswordCredentials artifactoryCreds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def artifactoryUser = artifactoryCreds.getUsername() 
        def artifactoryPass = Secret.toString(artifactoryCreds.getPassword())
        StandardUsernamePasswordCredentials muleRepoCreds = CredentialsProvider.findCredentialById("MULEREPO_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def muleRepoUser = muleRepoCreds.getUsername() 
        def muleRepoPass = Secret.toString(muleRepoCreds.getPassword())
        container('maven-java8') {
            dir("${buildconfig.workingdir}") {
                writeFile file: 'mule-maven-settings.xml', text: libraryResource('mule-maven-settings.xml')
                sh """
                    set +x
                    ls
                    cd "${buildconfig.workingdir}"
                    sed -i -e 's/#muleRepoUser#/$muleRepoUser/g'  mule-maven-settings.xml
                    sed -i -e 's/#muleRepoPass#/$muleRepoPass/g'  mule-maven-settings.xml
                    set -x
                """
                // sh """
                //     cd "${buildconfig.workingdir}"
                //     mvn sonar:sonar -Dsonar.host.url=http://${buildconfig.sonarserver}:9000
                // """
            }
        }
    } // end return
}
def unittests(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforeTest", Object)) {
            buildconfig.proj.beforeTest(buildconfig)
        }
        container('maven-java8') {
            dir("${buildconfig.workingdir}") {
                // sh """
                //     cd "${buildconfig.workingdir}"
                    
                //     mvn test "-Dcoverage.threshold=${buildconfig.coverage_threshold}" -s mule-maven-settings.xml
                // """
                
                def exists = fileExists 'target/munit-reports/coverage/summary.html'
                if (exists) 
                {
                    echo 'html report file is generated'
                    publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'target/munit-reports/coverage',
                    reportFiles: 'summary.html',
                    reportName: "Coverage Report" ])
                }
            }
        }
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterTest", Object)) {
            buildconfig.proj.afterTest(buildconfig)
        }
    } // end return
}
def build(Object buildconfig) {
    return {
        StandardUsernamePasswordCredentials artifactoryCreds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def artifactoryUser = artifactoryCreds.getUsername() 
        def artifactoryPass = Secret.toString(artifactoryCreds.getPassword())
        StandardUsernamePasswordCredentials muleRepoCreds = CredentialsProvider.findCredentialById("MULEREPO_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def muleRepoUser = muleRepoCreds.getUsername() 
        def muleRepoPass = Secret.toString(muleRepoCreds.getPassword())
        container('maven-java8') {
            dir("${buildconfig.workingdir}") {
                writeFile file: 'mule-maven-settings.xml', text: libraryResource('mule-maven-settings.xml')
                sh """
                    set +x
                    ls
                    cd "${buildconfig.workingdir}"
                    sed -i -e 's/#muleRepoUser#/$muleRepoUser/g'  mule-maven-settings.xml
                    sed -i -e 's/#muleRepoPass#/$muleRepoPass/g'  mule-maven-settings.xml
                    set -x
                """
                 sh """
                     cd "${buildconfig.workingdir}"
                     mvn -DskipTests clean package -s mule-maven-settings.xml
                    """
            }
        }
    } // end return
}

def postbuild(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterBuild", Object)) {
            buildconfig.proj.afterBuild(buildconfig)
        }
    } // end return
}

def publish(Object buildconfig) {
   return {
       if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforePush", Object)) {
           buildconfig.proj.beforePush(buildconfig)
       }
       container('devopstoolsimage') {
               withCredentials([[

                                $class          : 'UsernamePasswordMultiBinding',
                                credentialsId   : "ARTIFACTORY_ACCESS",
                                usernameVariable: 'ARTIFACTORY_USERID',
                                passwordVariable: 'ARTIFACTORY_PASSWORD'
                            ]]) {

               dir("${buildconfig.workingdir}") {
                    sh """
                        cd target
                        mv *.zip ${buildconfig.repoName}.zip
                        cd ..
                    """

                    sh """
                        curl --fail -u ${ARTIFACTORY_USERID}:${ARTIFACTORY_PASSWORD} \
                            --header "X-Checksum-MD5:\$(md5sum target/${buildconfig.repoName}.zip |awk '{print \$1}')" \
                            --header "X-Checksum-Sha1:\$(sha1sum target/${buildconfig.repoName}.zip |awk '{print \$1}')" \
                            -X PUT \"http://${buildconfig.arttifactserver}/artifactory/libs-snapshot-local/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}.zip\" \
                            -T "target/${buildconfig.repoName}.zip"
                        """
                    buildconfig["build_artifact"] = "http://${buildconfig.arttifactserver}/artifactory/libs-snapshot-local/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}.zip"

                   sh """
                   pwd
                   cd terraform
                   wget https://releases.hashicorp.com/packer/1.3.4/packer_1.3.4_linux_amd64.zip
                   unzip packer_1.3.4_linux_amd64.zip
                   ./packer version
                   ./packer validate -var-file=variables.json -var 'artifact=${buildconfig.build_artifact}' mule_packer.json
                   ./packer build -force -var-file=variables.json -var 'artifact=${buildconfig.build_artifact}' mule_packer.json
                   ls
                        curl --fail -u ${ARTIFACTORY_USERID}:${ARTIFACTORY_PASSWORD} \
                            --header "X-Checksum-MD5:\$(md5sum manifest.json |awk '{print \$1}')" \
                            --header "X-Checksum-Sha1:\$(sha1sum manifest.json |awk '{print \$1}')" \
                            -X PUT \"http://${buildconfig.arttifactserver}/artifactory/libs-snapshot-local/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}-manifest.json\" \
                            -T "manifest.json"
                        """
                    buildconfig["build_manifest"] = "http://${buildconfig.arttifactserver}/artifactory/libs-snapshot-local/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}-manifest.json"
                    
               }
           }    
       }
       if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterPush", Object)) {
           buildconfig.proj.afterPush(buildconfig)
       }
   } // end return
}

return this