package com.devops

import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret



import hudson.plugins.git.*
import hudson.model.BuildAuthorizationToken
import org.apache.commons.lang.RandomStringUtils
import hudson.tasks.Mailer
import com.cloudbees.hudson.plugins.folder.Folder
import hudson.scm.SCM
import hudson.model.ListView
import hudson.views.ListViewColumn
import hudson.triggers.SCMTrigger
import hudson.*
import hudson.security.*
import java.util.*
import com.michelin.cio.hudson.plugins.rolestrategy.*
import java.lang.reflect.*

def createJenkinsRole(String projectName) {
    def authStrategy = Hudson.instance.getAuthorizationStrategy()
    if(authStrategy instanceof RoleBasedAuthorizationStrategy){
        RoleBasedAuthorizationStrategy roleAuthStrategy = (RoleBasedAuthorizationStrategy) authStrategy
        // Make constructors available
        Constructor[] constrs = Role.class.getConstructors();
        for (Constructor<?> c : constrs) {
            c.setAccessible(true);
        }
        // Make the method assignRole accessible
        Method assignRoleMethod = RoleBasedAuthorizationStrategy.class.getDeclaredMethod("assignRole", String.class, Role.class, String.class);
        assignRoleMethod.setAccessible(true);

        // Create role
        Set<Permission> permissions = new HashSet<Permission>();
        permissions.add(Permission.fromId("hudson.model.Item.Read"));
        permissions.add(Permission.fromId("hudson.model.Item.Build"));
        permissions.add(Permission.fromId("hudson.model.Item.Workspace"));
        permissions.add(Permission.fromId("hudson.model.Item.Cancel"));
        // The release permission is only available when the release plugin is installed
        String releasePermission = Permission.fromId("hudson.model.Item.Release");
        if (releasePermission != null) {
            permissions.add(releasePermission);
        }
        permissions.add(Permission.fromId("hudson.model.Run.Delete"));
        permissions.add(Permission.fromId("hudson.model.Run.Update"));
        Role newRole = new Role(projectName.toLowerCase(), projectName.toLowerCase() + "-.*", permissions);
        roleAuthStrategy.addRole(RoleBasedAuthorizationStrategy.PROJECT, newRole);

        // assign the role
        //roleAuthStrategy.assignRole(RoleBasedAuthorizationStrategy.PROJECT, newRole, ldapGroupName);
        Jenkins.instance.save()
        println "OK"
    }
    else {
        println "Role Strategy Plugin not found!"
    }

}

def cloneSrcRepo(String srcGit,String srcGitBranch) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()
    def gitPass = Secret.toString(creds.getPassword())

    def gitUserUri=gitUser.replace("@", "%40")
    dir ("$workspace") {
       // bat """
       //     RMDIR ${srcGit} /S /Q
       //     git clone https://${gitUserUri}:${gitPass}@bitbucket.org/cfsintnadev/${srcGit}.git --depth=1 ${srcGit}
       // """

         sh """
             rm -rf "${srcGit}"
             git clone -b ${srcGitBranch} https://${gitUserUri}:${gitPass}@bitbucket.org/${srcGit}.git --depth=1 '${srcGit}'
         """

        // base project url
        //https://cfsintna@bitbucket.org/cfsintnadev/base-java-app.git
    }   

}

def createNewRepo(String projectName, String destGit) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()
    def gitPass = Secret.toString(creds.getPassword())
    def repositoryName = destGit.split("/").last()
    println repositoryName
    //hardcoded User value = vrnvvikas 
    //def outputCreateNewRepo = shWithOutput 
    sh """curl -X POST -u "${gitUser}:${gitPass}" -H "Content-Type: application/json" -d '{ 
        "scm": "git", 
        "is_private": true
        }' https://api.bitbucket.org/2.0/repositories/vrnvikas1994/${repositoryName}"""
    //curl -X POST -u ${gitUser}:${gitPass} -H "Content-Type: application/json" -d '{ "scm": "git", "is_private": true }' https://api.bitbucket.org/2.0/repositories/${gitUser}/${destGit.split("/").last()}
    //curl --user ${gitUser}:${gitPass} https://api.bitbucket.org/2.0/repositories/ --data name=${destGit.split("/").last()} --data is_private=true 
    //assert !outputCreateNewRepo.contains("This repository URL is already taken by ") : "Repository $destGit already exists. Please use another repo name or check the environment name being created"
}

def syncSrcToDestRepo(String srcGit, String destGit) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()
    def gitPass = Secret.toString(creds.getPassword())

    def gitUserUri=gitUser.replace("@", "%40")
    println srcGit
    dir ("${workspace}") {
        sh """
            set -e
            cd "${srcGit}"
            ls -al
            rm -rf .git
            git init
            git config user.name "vrnvikas1994"
            git config user.email vrnvikas1994@gmail.com
            git add README.md JenkinsfileLoader.groovy build.yaml
            git commit -m "Master Commit"
            git push https://${gitUserUri}:${gitPass}@bitbucket.org/vrnvikas1994/${destGit}.git master
            git checkout -b develop
            git add -A .
            git commit -m "Initial check-in from Base Repo"
            git remote add origin https://${gitUserUri}:${gitPass}@bitbucket.org/vrnvikas1994/${destGit}.git
            git push https://${gitUserUri}:${gitPass}@bitbucket.org/vrnvikas1994/${destGit}.git develop
        """
    }

}

def createNewJenkinsView(String projectName) {
    jobDsl additionalParameters: [
        projectName: projectName
    ], scriptText: '''
        // Create the view
        listView("${projectName}") {
            jobs {
                regex("^${projectName} Projects/.*\\\$")
            }
            columns {
                status()
                weather()
                name()
                lastSuccess()
                lastFailure()
                lastDuration()
            }
            recurse(true)
        }
    '''
}

def createNewJenkinsFolder(String projectsFolder, String projectName) {
    jobDsl additionalParameters: [
        projectsFolder: projectsFolder,
        projectName: projectName
    ], scriptText: '''
        // Get/Create the folder
        folder(projectsFolder) {
            description("Folder for project ${projectName}")
        }
    '''
}

def createNewJenkinsJobWithMultiBranch(String projectsFolder, String projectName, String destProject, String destGit) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()

    jobDsl additionalParameters: [
        projectsFolder: projectsFolder,
        projectName: projectName,
        destProject: destProject,
        destGit: destGit,
        gitUserUri: gitUser.replace("@", "%40")
    ], scriptText: '''
    multibranchPipelineJob("${projectsFolder}/${destProject}") {
            branchSources {
                        git {
                            remote("https://${gitUserUri}@bitbucket.org/vrnvikas1994/${destGit}.git")
                            credentialsId("GIT_ACCESS")
                            includes("master develop feature/* bugfix/* release/* hotfix/*")
                            }
                        }
  
            orphanedItemStrategy {
            discardOldItems {
            numToKeep(10)
            }
        }
  
            triggers {
            periodic(2)
            }
  
            configure {
            it / factory(class: 'org.jenkinsci.plugins.workflow.multibranch.WorkflowBranchProjectFactory') {
            owner(class: 'org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject', reference: '../..')
            scriptPath("JenkinsfileLoader.groovy")
              }
              }    
}
'''
}

def createNewJenkinsJob(String projectsFolder, String projectName, String destProject, String destGit) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()

    jobDsl additionalParameters: [
        projectsFolder: projectsFolder,
        projectName: projectName,
        destProject: destProject,
        destGit: destGit,
        gitUserUri: gitUser.replace("@", "%40")
    ], scriptText: '''
        pipelineJob("${projectsFolder}/${destProject}") {
            definition {
                cpsScm {
                    scm {
                        git {
                            remote {
                                url("https://${gitUserUri}@bitbucket.org/vrnvikas1994/${destGit}.git")
                                credentials("GIT_ACCESS")
                            }
                            extensions {
                                cloneOptions {
                                    depth(1)
                                    shallow(true)
                                }
                            }
                            branch("*/master")
                        }
                    }
                    scriptPath('JenkinsfileLoader.groovy')
                    lightweight(true)
                }
            }
            triggers {
                scm('H/5 * * * *')
            }
        }
    '''
}

def createNewJenkinsDeployJob(String projectsFolder, String projectName, String destProject, String destGit) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()

    jobDsl additionalParameters: [
        projectsFolder: projectsFolder,
        projectName: projectName,
        destProject: destProject,
        destGit: destGit,
        gitUserUri: gitUser.replace("@", "%40"),
        inlineScript: '''
import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
import java.text.SimpleDateFormat
import groovy.json.JsonSlurper

def returnList = [ "latest" ]
try {
 def parentProjectName = project.getName() - ~/(?i)-deploy.*$/
 def parentFolder = project.getParent()
 def parentProject = parentFolder.getItem(parentProjectName)
 hudson.model.Run anyRecentBuild
 if (parentProject instanceof org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject) {
   anyRecentBuild = parentProject.getAllJobs().first().getLastBuild()
 } else {
   def parentProjectBuilds = parentProject.getBuilds()
   anyRecentBuild = parentProjectBuilds[0]
 }
 StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS_1",
     StandardUsernamePasswordCredentials.class, anyRecentBuild)
 def artifactoryUser = creds.getUsername()
 def artifactoryPass = Secret.toString(creds.getPassword())

 def apiUrl = "http://ec2-34-220-18-182.us-west-2.compute.amazonaws.com:8081/artifactory/api/search/aql".toURL().openConnection()
 def basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary("${artifactoryUser}:${artifactoryPass}".getBytes())
 apiUrl.setRequestMethod("POST")
 apiUrl.setRequestProperty("Authorization", basicAuth)
apiUrl.setRequestProperty("content-type", "text/plain; charset=utf-8")
 apiUrl.setDoOutput(true)
 def writer = new OutputStreamWriter(apiUrl.outputStream)
 def query = 'items.find({"repo":{"$eq":"libs-snapshot-local"},"path":{"$match": "' + parentProjectName +  '/*"}})'
 writer.write(query)
 writer.flush()
 writer.close()
 apiUrl.connect()
 def json = new JsonSlurper().parse(apiUrl.getInputStream())
 def sortedJson = json.results.toSorted { a, b -> b.created <=> a.created }
 def dateFormat = new SimpleDateFormat("yyyy-MM-DD'T'HH:m:s.S")
 sortedJson.each {
   def pathParts = it.path.tokenize('/')
   def createdDate =  dateFormat.parse(it.created).format("yyyy-MM-DD    h:ma")
   if (pathParts[2]) {
     returnList.push("${createdDate} -- ${pathParts[1]} -- ${pathParts[2]}")
   } else {
     returnList.push("${createdDate} -- ${pathParts[1]}")
   }
 }
} catch(e) {
 returnList.push(e)
 if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException) {
   throw e
 }
 println e
}
return returnList
'''
    ], scriptText: '''
        pipelineJob("${projectsFolder}/${destProject}-deploy-dev") {
            definition {
                cpsScm {
                    scm {
                        git {
                            remote {
                                url("https://${gitUserUri}@bitbucket.org/vrnvikas1994/${destGit}.git")
                                credentials("GIT_ACCESS")
                            }
                            extensions {
                                cloneOptions {
                                    depth(1)
                                    shallow(true)
                                }
                            }
                            branch("develop")
                        }
                    }
                    scriptPath('JenkinsfileDeployer.groovy')
                    lightweight(true)
                }
            }
            parameters {
                booleanParam('ApplyChanges', true, 'In high-risk environments, you should check the terraform plan first.')
                stringParam('ConfigFile', 'dev/dev1.tf', 'The terraform configuration file that should be used.')
            }
            configure {
                project->
                    project / 'properties' / 'hudson.model.ParametersDefinitionProperty' / parameterDefinitions << 'jp.ikedam.jenkins.plugins.extensible__choice__parameter.ExtensibleChoiceParameterDefinition' (plugin: "extensible-choice-parameter") {
                        name 'BuildToDeploy'
                        description 'The build number to deploy or "latest".'
                        editable 'false'
                        choiceListProvider (class: 'jp.ikedam.jenkins.plugins.extensible_choice_parameter.SystemGroovyChoiceListProvider') {
                            usePredefinedVariables 'true'
                            groovyScript (plugin: 'script-security') {
                                sandbox 'true'
                                script inlineScript
                            } // groovyScript
                        } // groovyScript
                    } // ExtensibleChoiceParameterDefinition
            } // configure
        }
    '''
}
def createNewJenkinsCapDeployJob(String projectsFolder, String projectName) {
    jobDsl additionalParameters: [
        projectsFolder: projectsFolder,
        projectName: projectName,
        inlineScript: """
@Library('CommonLib@master') _
def common = new CommonDeployPipeline()
common.runPipeline("${projectsFolder}", "${projectName}")
        """
    ], scriptText: '''
        pipelineJob("${projectsFolder}/${projectName} Capability Deploy") {
            definition {
                cps {
                    script(inlineScript)
                }
            }
        }
    '''
}

def createSonarProject(String destProject) {



}

// executes jenkins sh command but returns the shell output
def shWithOutput(String input) {
    def result  = sh (
        script: input,
        returnStdout: true
    ).trim()
    return result
}

// gets the email address of the current jenkins user TODO return object with name
def getCurrentUserEmail() {
    def result = null
    def cause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause.class)
    def id = cause.getUserId()
    User u = User.get(id)
    def umail = u.getProperty(Mailer.UserProperty.class)
    result = umail.getAddress()
    return result
}

return this
