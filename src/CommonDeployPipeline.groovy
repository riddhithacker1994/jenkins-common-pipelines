import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret

import java.net.URLConnection
import java.io.InputStream
import org.yaml.snakeyaml.Yaml

def runPipeline() {

    def commonDeployer = new CommonDeployer()
    def jmeterTest = new JMeterTestpipeline()
    def postmanTest = new PostmanNewmanPipeline()
    def smokeTest = new SmokeTestPipeline()
    def soapuiTest = new SoapUITestPipeline()
    def deployData = []
    def k8slabel = "jenkins-pipeline-${UUID.randomUUID().toString()}"
    def functionalTestReportName
    def performanceTestReportName

    def jobName = "${JOB_BASE_NAME}"
        .toLowerCase()
        .replace(" ", "-")
        .replace(".net", "dotnet")

    def deployEnv = jobName.substring(jobName.lastIndexOf("-") + 1)
    print deployEnv
    jobName = jobName.substring(0, jobName.lastIndexOf("-deploy"))
    println jobName

    def buildToDeploy = params["BuildToDeploy"].toLowerCase()
    def deployRoleArn = ""
    def baseUrl,scmUrl, scmBrowseUrl, scmBrowseBranch, scmBrowseBuildFileUrl, buildFile, buildFileData, scmBrowseTFPath, scmBrowseConfigFileUrl, tfConfigFile, deployRoleArnMatch, git_commit_short_head
    try {
      // Get the Browse SCM url
      scmUrl = scm.getUserRemoteConfigs()[0].getUrl()
      scmBrowseUrl = scmUrl.replaceAll(/https:\/\/[^@]*@([^\/]*)\/scm\/([^\/]*)\/([^\.]*)\.git/, 'https://$1/projects/$2/repos/$3/browse')
      scmBrowseBranch = scm.branches[0].name.replaceAll(/^\*\//, '')

      print "scmBrowseBranch : ${scmBrowseBranch}"
      // Get the Terraform config path
      //scmBrowseBuildFileUrl = "${scmBrowseUrl}/build.yaml"

      baseUrl = "https://bitbucket.org/vrnvikas1994/${jobName}"

      deployRoleArn = "arn:aws:iam::031722342363:role/DevSecOpsAdminRolePolicy"


    } catch (e) {
      print "ExceptionMessage"
      print e.getMessage()
    }

    if (deployRoleArn.length() < 1) {
        throw new Exception("Deployment ARN not found.")
    }
    deployRoleArnMatch = null // deployRoleArnMatch isn't serializable

    println "Running deployment as ${deployRoleArn}"

    println "K8SLabel: ${k8slabel}"
    podTemplate(label: "${k8slabel}", containers: [
    containerTemplate(name: 'devopstoolsimage', image: 'vrnvikas1994/devops-tools-deploy', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'docker', image: 'docker:latest', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'jmeter', image: 'justb4/jmeter', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'postman', image: 'vrnvikas1994/postman', ports: [portMapping(name: 'postman', containerPort: 8093, hostPort: 8093)],ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'soapui', image: 'ddavison/soapui', ports: [portMapping(name: 'soapui', containerPort: 3000, hostPort: 3000)],ttyEnabled: true)],
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')],
    annotations: [podAnnotation(key: "iam.amazonaws.com/role", value: "${deployRoleArn}")]
    ) {
    node (k8slabel) {
        currentBuild.result = 'SUCCESS'
        try {
            stage ("Git Pull") {

                // Get deployment package
                //commonDeployer.deployment_artifact_pull(jobName)
                //deployData = readYaml file: "deployment.yaml"
                //println deployData

                // Doing This becouse Artifactory not intergrated for deployment package strategy
                checkout scm

                dir("${workspace}") {
                    println "Setting Variables"
                    withCredentials([[
                        $class: 'UsernamePasswordMultiBinding',
                        credentialsId: "GIT_ACCESS",
                        usernameVariable: 'GIT_ACCESS_USERID',
                        passwordVariable: 'GIT_ACCESS_PASSWORD'
                    ]]) {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            sh """git config --global credential.helper '!f() { sleep 1; echo "username=${GIT_ACCESS_USERID}\npassword=${GIT_ACCESS_PASSWORD}"; }; f'
                            """
                            def git_commit_hash_head = sh (script: "git rev-parse --verify HEAD", returnStdout: true).trim()
                            
                            scmBrowseBuildFileUrl = "${baseUrl}/raw/${git_commit_hash_head}/build.yaml"

                            //buildFile = commonDeployer.getSingleFileFromGit(scmBrowseBuildFileUrl, scmBrowseBranch)

                            //buildFileData = new Yaml().load(buildFile)
                            buildFileData = readYaml file: "build.yaml"
                            println"buildFileData = ${buildFileData}"
                            scmBrowseTFPath = buildFileData.deployment.terraform_path

                            // Get the configuration file
                            //scmBrowseConfigFileUrl = "${scmBrowseUrl}/${scmBrowseTFPath}/configurations/${params["ConfigFile"]}"
                            scmBrowseConfigFileUrl = "${baseUrl}/raw/${git_commit_hash_head}/terraform/configurations/dev/dev1.tf"

                            //tfConfigFile = commonDeployer.getSingleFileFromGit(scmBrowseConfigFileUrl, scmBrowseBranch)

                            // Get the deploy role arn
                            //deployRoleArnMatch = tfConfigFile =~ /(?m)^.*deploy_role_arn[^"]*"([^"]*)".*$/

                            //deployRoleArn = deployRoleArnMatch[0][1]

                            //deployRoleArnMatch = null // deployRoleArnMatch isn't serializable

                            git_commit_short_head = sh (script: "git rev-parse --short=6 HEAD", returnStdout: true).trim()

                            if (buildToDeploy == 'latest') {
                              //query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' +  reponame + '/*"}})'
                              print "buildToDeploy : 0.1-${git_commit_short_head}"
                            } else {
                                git_commit_short_head = buildToDeploy.split(" -- ").last().split("_").last()
                                print "buildToDeploy : 0.1-${git_commit_short_head}"
                                //query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' + reponame + '/*/' + buildToDeploy + '"}})'
                            }

                            print "git_commit_short_head : ${git_commit_short_head}"

                        }
                    }
                }

            }

            // We are not doing true parallel due to limitations in visualizaions:
            // see https://issues.jenkins-ci.org/browse/JENKINS-38442

            // Pre build tasks
            stage ("Artifact Pull") {
                // parallel commonDeployer.generateSteps("Artifact Pull", deployData) {
                //     return (it.buildtype == "lambda") ? commonDeployer.lambda_artifact_pull(it, deployData) : commonDeployer.docker_artifact_pull(it, deployData)
                // }
                //println "${buildFileData}"

                
                if(buildFileData.apigee != null) {
                    println "In Artifact Pull Stage"
                    buildFileData.apigee.each {buildFileData
                        def deploymentdata = [:]
                        deploymentdata.jobName = jobName
                        deploymentdata.artifactName = "${jobName}-${it.name}"
                        deploymentdata.artifact_server_url = buildFileData.deployment.artifact_server_url
                        deploymentdata.terraform_path = buildFileData.deployment.terraform_path
                        deploymentdata.version = buildFileData.version
                        deploymentdata.git_commit_short_head = git_commit_short_head
                        deploymentdata.branch = scmBrowseBranch
                        println deploymentdata
                        commonDeployer.muleonprem_artifact_pull(deploymentdata)
                        deployData = deploymentdata
                    }
                }

                if(buildFileData.docker != null) {
                    println "In Artifact Pull Stage"
                    buildFileData.docker.each {buildFileData
                        def deploymentdata = [:]
                        deploymentdata.jobName = jobName
                        deploymentdata.artifactName = "${jobName}-${it.name}"
                        deploymentdata.artifact_server_url = buildFileData.deployment.artifact_server_url
                        deploymentdata.terraform_path = buildFileData.deployment.terraform_path
                        deploymentdata.version = buildFileData.version
                        deploymentdata.git_commit_short_head = git_commit_short_head
                        deploymentdata.branch = scmBrowseBranch
                        println deploymentdata
                        deployData = deploymentdata
                        //commonDeployer.muleonprem_artifact_pull(deploymentdata)
                    }
                }


                if(buildFileData.muleonprem != null) {
                    println "In Artifact Pull Stage"
                    buildFileData.muleonprem.each {buildFileData
                        def deploymentdata = [:]
                        deploymentdata.jobName = jobName
                        deploymentdata.artifactName = "${it.name}"
                        deploymentdata.artifact_server_url = buildFileData.deployment.artifact_server_url
                        deploymentdata.terraform_path = buildFileData.deployment.terraform_path
                        deploymentdata.version = buildFileData.version
                        deploymentdata.git_commit_short_head = git_commit_short_head
                        deploymentdata.branch = scmBrowseBranch
                        println deploymentdata
                        //deployData.push(deploymentdata)
                        deployData = deploymentdata
                        println"${deployData.version}"
                    }
                }

                if(buildFileData.muleonpremnew != null) {
                    println "In Artifact Pull Stage of muleonpremnew"
                    buildFileData.muleonpremnew.each {buildFileData
                        def deploymentdata = [:]
                        deploymentdata.jobName = jobName
                        deploymentdata.artifactName = "${it.name}"
                        deploymentdata.artifact_server_url = buildFileData.deployment.artifact_server_url
                        deploymentdata.terraform_path = buildFileData.deployment.terraform_path
                        deploymentdata.version = buildFileData.version
                        deploymentdata.git_commit_short_head = git_commit_short_head
                        deploymentdata.branch = scmBrowseBranch
                        println deploymentdata
                        //deployData.push(deploymentdata)
                        commonDeployer.get_AMI_ID(deploymentdata)
                        println "Called get AMI ID"
                        println"${deploymentdata.ami_id}"
                        deployData = deploymentdata
                        }

                    }
                
            }

            // Deployment Scan
            stage ("Pre deployment callback") {
                commonDeployer.terraform_scan("${workspace}/${scmBrowseTFPath}")
            }

            // Plan or Apply
             stage ("Deployment") {
                 commonDeployer.terraform_apply(
                         params["ApplyChanges"],
                         "${workspace}/${scmBrowseTFPath}",
                         params["ConfigFile"],
                         jobName,
                         git_commit_short_head,     //deployData.git_hash.take(6),
                         (buildFileData.components.collect {cur -> return cur.name
                         }).join(","), // Create a comma seperated string of build names
                         (buildFileData.components.collect {
                           cur -> return (cur.buildtype == "docker") ? cur.build_artifact : cur.build_artifact
                         }).join(","), // Create a comma seperated string of build artifacts
                         deployData, //deployData.version, //Passing whole build file
                         scmBrowseBranch
                 )
             }

           // Run Smoke Test
           stage ("Smoke Test") {
             if (buildFileData.smoke_test ) {
               smokeTest.smoketest_run(deployEnv)
             } else {
               println("Skipping Smoke Test")
             }
           }

           // Run Functional test
           stage ("Functional Test Run") {
             if (buildFileData.postman_newman_path ) {
                functionalTestReportName = "Postman_20Report"
                postmanTest.postmantest_run(buildFileData.postman_newman_path,deployEnv)
             } else if(buildFileData.soapui_path){
                functionalTestReportName = "testReport"
                soapuiTest.soapuitest_run(buildFileData.soapui_path,deployEnv)
             } else {
               println("No functional test path found. Functional Test was not run.")
             }
           }

           // Run Performance Test
           stage ("Performance Test Run") {
             if (buildFileData.jmeter_path ) {
                performanceTestReportName = "Jmeter_20Summary_20Report"
                jmeterTest.jmetertest_run(buildFileData.jmeter_path,deployEnv)
             } else {
               println("No Performance test path found. Performance test did not run.")
             }
           }


           stage ("Tag Artifact"){
            
            if(buildFileData.components != null) {
                println "${buildFileData}"    
                println "No artifacts tagged as it is a docker deployment"
            }
            else{
             if(params["ApplyChanges"]){
                def commonEmail = new CommonEmail()
               newProperties = [
                 'deployedToAWSEnv': [params["ConfigFile"].split("/")[0]],
                 'deployjob.number': [env.BUILD_NUMBER],
                 'deployjob.name': [env.JOB_NAME],
                 'commit': [git_commit_short_head],
                 'deployedBy': [commonEmail.getJobSubmitter()]
               ]

               println "New Properties"
               println newProperties
               
               println "Getting ArtifactProperties"
               // currentProperties = commonDeployer.getArtifactProperties(deployData)
               // println currentProperties

               // currentProperties.each { key, value ->
               //   if (newProperties[key]) {
               //     newProperties[key] = newProperties[key].toSet()
               //     newProperties[key].addAll(value)
               //   }
               // }
               println "${deployData}"
               println "Putting ArtifactProperties"
               commonDeployer.putArtifactProperties(deployData,newProperties)

             } else {
               println("Artifact not tagged because ApplyChanges is not set")
             }
           }
       }

        } catch (e) {
            if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException) {
                throw e
            }
            currentBuild.result = 'FAILURE'
            println "ERROR Detected:"
            println e.getMessage()
            def sw = new StringWriter()
            def pw = new PrintWriter(sw)
            e.printStackTrace(pw)
            println sw.toString()
        } finally {

            def commonEmail = new CommonEmail()
            def mailContent = ""

            //Adding test links to mail body for mule base project only as of now
            if (buildFileData.muleonprem != null || buildFileData.apigee != null || buildFileData.muleonpremnew != null) {
                mailContent =  
                """
                 <div class='content'>
                   <h1>Functional Tests</h1>
                   <table class="console">
                     <p> Postman Functional Test Results available at</p>
                     <a href="${BUILD_URL}${functionalTestReportName}/">${BUILD_URL}${functionalTestReportName}/</a>
                    </table>
                 </div>
                 """ +
                """
                 <div class='content'>
                   <h1>Performance Tests</h1>
                  <table class="console">
                     <p>Jmeter Performance Test Results available at</p> <br>
                     <p>Performance Report</p> <br>
                     <a href="${BUILD_URL}performance/">${BUILD_URL}performance/</a> <br>
                     <p>Summary Report</p> <br>
                     <a href="${BUILD_URL}${performanceTestReportName}/">${BUILD_URL}${performanceTestReportName}/</a>
                    </table>
                 </div>
                 """
            }



            commonEmail.sendDeployToJobSubmitter(mailContent)

        }
    } // node (k8slabel)
    } // podTemplate

}

return this
