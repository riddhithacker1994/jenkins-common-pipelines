

def runPipeline() {

  def dockerBuilder = new DockerBuilder()
  def muleOnPremBuilder = new MuleOnPremBuilder()
  def muleOnPremNewBuilder = new MuleOnPremNewBuilder()
  def muleDockerBuilder = new MuleDockerBuilder()
  def apigeeBuilder = new ApigeeBuilder()
  def tmpBuilddata = [:]
  def builddata = []
  def repoName = ""
  def buildVersion = ""
  def artifactoryServer
  def branchName = "${env.BRANCH_NAME}".replace("/", "-").toLowerCase()
  if (!branchName) {
    branchName = "master"
  }
  def k8slabel = "jenkins-pipeline-${UUID.randomUUID().toString()}"
  def hasdocker = false

  println "K8SLabel: ${k8slabel}"

  podTemplate(label: k8slabel, containers: [
    containerTemplate(name: 'devopstoolsimage', image: 'vrnvikas1994/devops-tools', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'maven-java8', image: 'vrnvikas1994/maven-java8-container', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'postman', image: 'vrnvikas1994/postman', ports: [portMapping(name: 'postman', containerPort: 8093, hostPort: 8093)],ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'docker', image: 'docker:latest', ttyEnabled: true, command: 'cat')],
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')]
    ) {
    node(k8slabel) {

      withCredentials([usernamePassword(credentialsId: 'ARTIFACTORY_ACCESS', passwordVariable: 'password', usernameVariable: 'username')]) {
       artifactoryServer = Artifactory.newServer url: "${artifact_host_url}", username: username, password: password
     }
      
      currentBuild.result = 'SUCCESS'
      try {
        stage("Git Pull") {
          def scmVars = checkout scm
          //config loading goes here
          repoName = scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
          print repoName

          tmpBuilddata = readYaml file: "build.yaml"
          buildVersion = tmpBuilddata.version

          if (!buildVersion) {
              throw new Exception("Project version not found.")
          }

          container('devopstoolsimage') {
            dir("${workspace}") {
              tmpBuilddata["git_hash"] =  sh(returnStdout: true, script: "git rev-parse HEAD").trim()
              tmpBuilddata["git_shorthash"] = sh(returnStdout: true, script: "git rev-parse --short=6 HEAD").trim()
              tmpBuilddata["git_commit"] = sh (script: "git log -1| sed 1d ", returnStdout: true).trim()
            }
          }

            // Set common variables for each build config
            println tmpBuilddata

            
            tmpBuilddata.docker.each {
                container('devopstoolsimage') {
                    dir("${workspace}") {
                        sh """
                            mkdir -p "devopsbuild/${it.name}"
                            ls |grep -v devopsbuild | while read -r file
                            do
                                echo "\$file"
                                cp -a "\$file" "devopsbuild/${it.name}/"
                            done
                        """
                    }
                }
                it.workingdir = "${workspace}/devopsbuild/${it.name}/${it.path}"
                it.proj = load "${it.workingdir}/DevOpsProject.groovy"
                it.branch = branchName
                it.jobname = "${env.JOB_NAME}".replace(" ", "-").replace(".NET", "dotnet").toLowerCase()
                it.repoName = repoName
                it.buildtype = "docker"
                it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
                builddata.push(it)
            }


            tmpBuilddata.muleonprem.each {
               it.workingdir = "${workspace}"
               it.proj = load "${it.workingdir}/DevOpsProject.groovy"
               it.branch = branchName
               it.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
               it.repoName = repoName
               it.buildtype = "muleonprem"
               it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
               it.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
               it.arttifactserver = tmpBuilddata.deployment.artifact_server_url
               it.sonarserver = tmpBuilddata.deployment.sonar_server_url
               it.coverage_threshold = 80
               builddata.push(it)
             }

            tmpBuilddata.muleonpremnew.each {
               it.workingdir = "${workspace}"
               it.proj = load "${it.workingdir}/DevOpsProject.groovy"
               it.branch = branchName
               it.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
               it.repoName = repoName
               it.buildtype = "muleonpremnew"
               it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
               it.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
               it.arttifactserver = tmpBuilddata.deployment.artifact_server_url
               it.sonarserver = tmpBuilddata.deployment.sonar_server_url
               it.coverage_threshold = 80
               builddata.push(it)
             }

             tmpBuilddata.muledocker.each {
               it.workingdir = "${workspace}/${tmpBuilddata.muledocker.path[0]}"
               it.proj = load "${it.workingdir}/DevOpsProject.groovy"
               it.branch = branchName
               it.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
               it.repoName = repoName
               it.buildtype = "muledocker"
               it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
               it.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
               it.coverage_threshold = 80
               builddata.push(it)
             }

             // new style for building info for multiple proxy
             if (tmpBuilddata.apigee != null) {
               def obj = tmpBuilddata.apigee[0]

               obj.workingdir = "${workspace}"
               obj.jobs = []
               tmpBuilddata.apigee.each {
                   def job = [:]

                   job.workingdir = "${workspace}"
                   job.proj = load "${workspace}/DevOpsProject.groovy"
                   job.branch = branchName
                   job.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
                   job.repoName = repoName
                   job.buildtype = "apigee"
                   job.lang = it.lang
                   job.pomDir = it.pom_dir
                   job.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
                   job.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
                   job.arttifactserver = tmpBuilddata.deployment.artifact_server_url
                   obj.jobs.push(job)
               }
               obj.buildtype = "apigee"
               //obj.version = "${buildVersion}-${tmpBuilddata["git_shorthash"]}"
               builddata.push(obj)

             }

             // old style for building info
             // tmpBuilddata.apigee.each {
             //   it.workingdir = "${workspace}"
             //   it.proj = load "${it.workingdir}/DevOpsProject.groovy"
             //   it.branch = branchName
             //   it.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
             //   it.repoName = repoName
             //   it.buildtype = "apigee"
             //   it.pomDir = tmpBuilddata.apigee.pom_dir[0]
             //   it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
             //   it.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
             //   it.arttifactserver = tmpBuilddata.deployment.artifact_server_url
             //   builddata.push(it)
             // }

            println "Builddata: ${builddata}"
            if (tmpBuilddata.docker.size() > 0) {
                hasdocker = true
            }
        }

        

        // Pre build tasks
        stage ("Pre build Callback") {
          parallel dockerBuilder.generateSteps("Pre Build", builddata) {
            switch (it.buildtype) {
              case "docker":
                return dockerBuilder.docker_prebuild(it)
              case "muleonprem":
               return muleOnPremBuilder.prebuild(it)
              case "muleonpremnew":
               return muleOnPremNewBuilder.prebuild(it)
              case "apigee":
               return apigeeBuilder.prebuild(it)
              case "muledocker":
               return muleDockerBuilder.prebuild(it)
              default:
               return {
                 println ("No Pre-build task for ${it.buildtype}")
               }
            }
          }
        }

        // Code Quaility Scan
        stage ("Code Quaility Check") {
          parallel dockerBuilder.generateSteps("Code Quaility Scan", builddata) {
            switch (it.buildtype) {
              
                case "docker":
                  return dockerBuilder.docker_code_quality(it)
                case "muleonprem":
                  return muleOnPremBuilder.code_quality(it)
                case "muleonpremnew":
                  return muleOnPremNewBuilder.code_quality(it)
                case "apigee":
                  return apigeeBuilder.code_quality(it)
                case "muledocker":
                  return muleDockerBuilder.code_quality(it)
            }
          }
        }

        // Unit Tests
        stage ("Unit Tests") {
          parallel dockerBuilder.generateSteps("Unit Tests", builddata) {
            switch (it.buildtype) {
              
                case "docker":
                  return dockerBuilder.docker_unittests(it)
                case "muleonprem":
                  return muleOnPremBuilder.unittests(it)
                case "muleonpremnew":
                  return muleOnPremNewBuilder.unittests(it)
                case "apigee":
                  return apigeeBuilder.unittests(it)
                case "muledocker":
                  return muleDockerBuilder.unittests(it)
            }
          }
        }

        // Build
        stage ("Build") {
          parallel dockerBuilder.generateSteps("Build", builddata) {
            switch (it.buildtype) {
              
                case "docker":
                  return dockerBuilder.docker_build(it)
                case "muleonprem":
                  return muleOnPremBuilder.build(it)
                case "muleonpremnew":
                  return muleOnPremNewBuilder.build(it)
                case "apigee":
                  return apigeeBuilder.build(it)
                case "muledocker":
                  return muleDockerBuilder.build(it)

            }
          }
        }



      // Post build tasks
      if ("${branchName}" =~ /^(?!feature)/) {
        stage ("Post build Callback") {
          parallel dockerBuilder.generateSteps("Postbuild", builddata) {
            switch (it.buildtype) {
              
              case "docker":
                return dockerBuilder.docker_postbuild(it)
              case "muleonprem":
                return muleOnPremBuilder.postbuild(it)
              case "muleonpremnew":
                return muleOnPremNewBuilder.postbuild(it)
              case "apigee":
                return apigeeBuilder.postbuild(it)
              case "muledocker":
                return muleDockerBuilder.postbuild(it)
            }
          }
        }
    }


      // Publish Artifacts
      if ("${branchName}" =~ /^(?!feature)/) {
        stage ("Push Artifact") {
          parallel dockerBuilder.generateSteps("Publish Artifacts", builddata) {
            switch (it.buildtype) {
              
              case "docker":
                return dockerBuilder.docker_publish(it)
              case "muleonprem":
                return muleOnPremBuilder.publish(it)
              case "muleonpremnew":
                return muleOnPremNewBuilder.publish(it)
              case "apigee":
                return apigeeBuilder.publish(it)
              case "muledocker":
                return muleDockerBuilder.publish(it)
            }
          }
        }
    }

    // Create Artifact Publish Info
    if ("${branchName}" =~ /^(?!feature)/) {
      stage ("Create Artifact Publish Info") {
        dir("${workspace}") {

          def uploadSpec = """{
               "files": [
                 {
                   "pattern": "${repoName}.zip",
                   "target": "libs-snapshot-local/${repoName}/${branchName}/${buildVersion}_${tmpBuilddata["git_shorthash"]}/"
                 }
               ]
             }"""
             println uploadSpec
             def buildInfo = Artifactory.newBuildInfo()
             artifactoryServer.upload(spec: uploadSpec, buildInfo: buildInfo)
             artifactoryServer.publishBuildInfo buildInfo
        }
      }
    }

      } catch (e) {
        if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException) {
          throw e
        }
        currentBuild.result = 'FAILURE'
        println "ERROR Detected:"
        println e.getMessage()
        def sw = new StringWriter()
        def pw = new PrintWriter(sw)
        e.printStackTrace(pw)
        println sw.toString()
      } finally {
        container('docker') {
          if (hasdocker) {
            sh 'docker system prune -a -f'// --filter "until=2h"'
          }
        }

        def commonEmail = new CommonEmail() 
        def mailContent = ""
        //Adding Munit Coverage to mail body for mule base project only as of now
        if (tmpBuilddata.muleonprem != null) {
          println "Adding munit-reports to mail body"
        mailContent =          """
         <div class='content'>
         <h1>Release Notes:</h1>
         <p>commit message : ${tmpBuilddata["git_commit"]} </p>
          <p><b>build version : ${buildVersion}_${tmpBuilddata["git_shorthash"]}</b></p>
          <h1>MUnit Tests</h1>
            <p>INTEGRATED MUNIT TESTS SUMMARY </p>
          """ +

          '''${FILE,path="target/munit-reports/coverage/summary.html"}''' 
        }
        commonEmail.sendBuildToGitCommitter(mailContent)

      }
    } // node (k8slabel)
  } // podTemplate


}

return this
