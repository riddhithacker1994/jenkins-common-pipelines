
def soapuitest_run(path,deployEnv) {
   if (fileExists('test.yaml')) {
     def soapuiTestData = readYaml file: "test.yaml"
     if (soapuiTestData.soapui != null) {
       soapuiTestData.soapui.each {
         def templateName = it.name
         def templatePath = it.path
         def env = it.env
         if(env == deployEnv){
           container('soapui') {
             dir("$workspace/${path}/") {
              if ( fileExists("${workspace}/${path}/${templatePath}/${templateName}.xml") ) {
                  sh """
                    mkdir reports
                    cp ${templatePath}/${templateName}.xml ${templateName}.xml
                    curl --form "project=@${templateName}.xml" --form "option=-f$workspace/${path}/reports" --form "option=-r" --form "option=-a" --form "option=-j" --form "option=-J" http://localhost:3000
                    sleep 10
                    cd reports
                    ls
                  """
                  junit '**/TEST-TestSuite_1.xml'
                  step([$class: 'ArtifactArchiver', artifacts: '**/*.xml'])
                  step([$class: 'ArtifactArchiver', artifacts: '**/*.txt'])
                  
              }else {
                 println("Soapui file not found --> ${templateName} did not execute")
               }
             }
           }
        }
       }
     }
     else {
       println "Testdata not found --> Soapui test did not execute."
     }
   }
}

return this