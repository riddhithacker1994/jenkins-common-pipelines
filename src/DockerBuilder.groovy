

def generateSteps(job, builddata, cl) {
    return builddata.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}

def docker_prebuild(Object buildconfig) {
    return {
        buildconfig.proj.beforeBuild(buildconfig)
    } // end return
}
def docker_code_quality(Object buildconfig) {
    return {
        def lc_lang = buildconfig["lang"].toLowerCase()
        print "commented"
    } // end return
}
def docker_unittests(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforeTest", Object)) {
            buildconfig.proj.beforeTest(buildconfig)
        }

        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterTest", Object)) {
            buildconfig.proj.afterTest(buildconfig)
        }
    } // end return
}
def docker_build(Object buildconfig) {
    return {
        container('docker') {
            dir("${buildconfig.workingdir}") {
                sh """
                    cd "${buildconfig.workingdir}"
					docker build --tag vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-latest ./
                    docker tag vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-latest vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-${buildconfig.version}
					"""
            }
        }
    } // end return
}
def docker_postbuild(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterBuild", Object)) {
            buildconfig.proj.afterBuild(buildconfig)
        }
    } // end return
}

def docker_publish(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforePush", Object)) {
            buildconfig.proj.beforePush(buildconfig)
        }
        container('docker') {
            withCredentials([[

                                 $class          : 'UsernamePasswordMultiBinding',
                                 credentialsId   : "DOCKER_ACCESS",
                                 usernameVariable: 'DOCKER_USERID',
                                 passwordVariable: 'DOCKER_PASSWORD'
                             ]]) {
                dir("${buildconfig.workingdir}") {
                    sh "docker login --username=${DOCKER_USERID} --password=${DOCKER_PASSWORD}"
                    sh "docker push vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-latest"
                    sh "docker push vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-${buildconfig.version}"
                    buildconfig["build_artifact"] = "vrnvikas1994/${buildconfig.repoName}:${buildconfig.branch}-${buildconfig.version}"
                }
            }
        }
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterPush", Object)) {
            buildconfig.proj.afterPush(buildconfig)
        }
    } // end return
}

return this
