
def smoketest_run(deployEnv) {
   if (fileExists('build.yaml'))
     {
     def buildInfo = readYaml file: "build.yaml"
     if (buildInfo.smoke_test != null) {
       buildInfo.smoke_test.each {
         def testUrl = it.url
         def testStatus = it.status
         def env = it.env
         if(env == deployEnv){
          container('devopstoolsimage') {
            dir("$workspace/") {              
                sh """
                  sleep 20
                  APP_STATUSCODE=\$(curl --silent --output /dev/stderr --write-out "%{http_code}" http://${testUrl})
                  if [ "\$APP_STATUSCODE" -eq ${testStatus} ]
                  then
                    echo "API is up and running"
                  else
                    echo "API validation failed"
                  exit 1
                  fi
                """
            }
          }
        }
       }
     }
     else {
       println "Testdata not found --> SmokeTest did not execute."
     }
   }
}

return this