
import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder


// Sorting requires NonCPS
@NonCPS
def getDeploymentArtifactUrl(reponame,artifactory_url) {
    def buildToDeploy = params["BuildToDeploy"].toLowerCase()
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def artifactoryUser = creds.getUsername()
    def artifactoryPass = Secret.toString(creds.getPassword())
    def apiUrl = "http://${artifactory_url}/artifactory/api/search/aql".toURL().openConnection()
    def basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary("${artifactoryUser}:${artifactoryPass}".getBytes())
    apiUrl.setRequestMethod("POST")
    apiUrl.setRequestProperty("Authorization", basicAuth)
    apiUrl.setRequestProperty("content-type", "text/plain; charset=utf-8")
    apiUrl.setDoOutput(true)
    def writer = new OutputStreamWriter(apiUrl.outputStream)
    def query
    if (buildToDeploy == 'latest') {
      query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' +  reponame + '/*"}})'
    } else {
      buildToDeploy = buildToDeploy.split(" -- ").last()
      query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' + reponame + '/*/' + buildToDeploy + '"}})'
    }
    println query
    writer.write(query)
    writer.close()
    apiUrl.connect()
    def json = new JsonSlurper().parse(apiUrl.getInputStream())
    ArrayList jsonResults = json.results
    ArrayList sortedJson = jsonResults.sort { a, b ->
      return b.created <=> a.created
    }
    println sortedJson
    def topBuild = sortedJson.first()
    return "https://artifactrepository.mcd.com/artifactory/vet-deployments/${topBuild.path}/${topBuild.name}"
}


def generateSteps(job, builddata, cl) {
    return builddata.components.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}

def getDeploymentArtifactUrl(String reponame) {
    
}

def terraform_scan(String terraformPath) {
    container('devopstoolsimage') {
        dir("${terraformPath}") {
            sh """
                
                ls -al
            """
        }
    }
}

def pre_deploy(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.mcdProj, "beforeDeploy", Object)) {
            buildconfig.proj.beforeDeploy(buildconfig)
        }
    } // end return
}

//TODO: Need to specify *standard* location of zip files etc. used by terraform so we can download using deployment.yaml
def terraform_apply(applyChanges, terraformPath, configFile, jobName, gitShortCode, buildNames, buildArtifacts, buildFile,scmBrowseBranch) {


    println """
        export TF_VAR_jobname=${jobName}
        export TF_VAR_gitshortcode=${gitShortCode}
        export TF_VAR_buildnames=${buildNames}
        export TF_VAR_buildartifacts=${buildArtifacts}
        export TF_VAR_buildversions=${scmBrowseBranch}-${buildFile.version}_${gitShortCode}
        export TF_VAR_artifact_type=${scmBrowseBranch}
        export TF_VAR_AMI_ID=${buildFile.ami_id}
    """
    def commonScript = """
        set -e

        export TF_VAR_jobname=${jobName}
        export TF_VAR_gitshortcode=${gitShortCode}
        export TF_VAR_buildnames=${buildNames}
        export TF_VAR_buildartifacts=${buildArtifacts}
        export TF_VAR_buildversions=${scmBrowseBranch}-${buildFile.version}_${gitShortCode}
        export TF_VAR_artifact_type=${scmBrowseBranch}
        export TF_VAR_AMI_ID=${buildFile.ami_id}

        export DATAFILE=configurations/${configFile}
        export ENVIRONMENT=\$(sed -nr 's/^\\s*environment\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
        export S3BUCKET=\$(sed -nr 's/^\\s*s3_bucket\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
        export S3BUCKETPROJ=\$(sed -nr 's/^\\s*s3_folder_project\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
        export S3BUCKETREGION=\$(sed -nr 's/^\\s*s3_folder_region\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
        export S3BUCKETTYPE=\$(sed -nr 's/^\\s*s3_folder_type\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
        export S3TFSTATEFILE=\$(sed -nr 's/^\\s*s3_tfstate_file\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)

        if      [ -z "\$ENVIRONMENT" ] ||
                [ -z "\$S3BUCKET" ] ||
                [ -z "\$S3BUCKETPROJ" ] ||
                [ -z "\$S3BUCKETREGION" ] ||
                [ -z "\$S3BUCKETTYPE" ] ||
                [ -z "\$S3TFSTATEFILE" ]
        then
            echo "S3 Bucket settings are not configured in the terraform configuration file."
            exit 1
        fi

        cat << EOF > "backend.tf"
terraform {
  backend "s3" {
    bucket = "\${S3BUCKET}"
    key    = "\${S3BUCKETPROJ}/\${S3BUCKETTYPE}/\${ENVIRONMENT}/\${S3TFSTATEFILE}"
    region = "\${S3BUCKETREGION}"
  }
}
EOF
        export AWS_DEFAULT_REGION=\${S3BUCKETREGION} &&
        export TF_VAR_aws_region=\${AWS_DEFAULT_REGION} &&
        aws sts get-caller-identity &&
        terraform init &&

        export KOPS_STATE_STORE=s3://kubernetes-mule-devops-cif-bucket && 
        export BASE_DOMAIN=cif.cluster.k8s.local &&
        export CLUSTER_DOMAIN="cif.cluster.k8s.local" &&
        kops export kubecfg "\${CLUSTER_DOMAIN}" &&
""";
//kubernetes-devops-cg-bucket
    if (applyChanges) {
        container('devopstoolsimage') {
            dir("${terraformPath}") {
                println "Applying"
                writeFile file: 'Devops-key-pair-cg-account.pem', text: libraryResource('Devops-key-pair-cg-account.pem')
				withCredentials([[
                    $class: 'UsernamePasswordMultiBinding',
                    credentialsId: "GIT_ACCESS",
                    usernameVariable: 'GIT_ACCESS_USERID',
                    passwordVariable: 'GIT_ACCESS_PASSWORD'
                ]]) {
					wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
						sh """git config --global credential.helper '!f() { sleep 1; echo "username=${GIT_ACCESS_USERID}\npassword=${GIT_ACCESS_PASSWORD}"; }; f'
						${commonScript}
						echo | terraform plan --var-file=\$DATAFILE
						echo | terraform apply --auto-approve --var-file=\$DATAFILE
						"""
					}
				}
            }
        }
    } else {
        container('devopstoolsimage') {
            dir("${terraformPath}") {
                println "Planning"
				withCredentials([[
                    $class: 'UsernamePasswordMultiBinding',
                    credentialsId: "GIT_ACCESS",
                    usernameVariable: 'GIT_ACCESS_USERID',
                    passwordVariable: 'GIT_ACCESS_PASSWORD'
                ]]) {
					wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
						sh """git config --global credential.helper '!f() { sleep 1; echo "username=${GIT_ACCESS_USERID}\npassword=${GIT_ACCESS_PASSWORD}"; }; f'
						${commonScript}
						echo | terraform plan --var-file=\$DATAFILE
						"""
					}
				}
            }
        }
    }
}

def post_deploy(Object buildconfig) {
    return {
        if (buildconfig.mcdProj.metaClass.respondsTo(buildconfig.mcdProj, "afterDeploy", Object)) {
            buildconfig.mcdProj.afterDeploy(buildconfig)
        }
    } // end return
}
def deployment_artifact_pull(jobname) {
    // StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
    //     StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    // def artifactoryUser = creds.getUsername()
    // def artifactoryPass = Secret.toString(creds.getPassword())

    container('devopstoolsimage') {
        dir("${workspace}") {
            // def url = getDeploymentArtifactUrl(jobname)
            // sh """
            //     curl --fail -u ${artifactoryUser}:${artifactoryPass} -o deployment.zip ${url}
            //     unzip deployment.zip
            //     cat deployment.yaml
            // """

        }
    }

}

def docker_artifact_pull(Object buildconfig, Object deploymentdata) {
    return {
    } // end return
}

def lambda_artifact_pull(Object buildconfig, Object deploymentdata) {
    return {
        // StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
        //         StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        // def artifactoryUser = creds.getUsername()
        // def artifactoryPass = Secret.toString(creds.getPassword())

        container('devopstoolsimage') {
            dir("${workspace}/${deploymentdata.terraform_path}") {
                // sh """
                //     curl --fail -u ${artifactoryUser}:${artifactoryPass} -o ${buildconfig.name}.zip ${buildconfig.build_artifact}
                // """
            }
        }

    } // end return
}

def muleonprem_artifact_pull(Object deploymentdata) {
   //return {
       StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
       StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
       def artifactoryUser = creds.getUsername()
       def artifactoryPass = Secret.toString(creds.getPassword())
       container('devopstoolsimage') {
            dir("${workspace}/${deploymentdata.terraform_path}") {
               sh """
                    echo "Downloading the artifact to deploy"
                    wget http://${deploymentdata.artifact_server_url}/artifactory/libs-snapshot-local/${deploymentdata.jobName}/${deploymentdata.branch}/${deploymentdata.version}_${deploymentdata.git_commit_short_head}/${deploymentdata.artifactName}.zip
               """
           }
       }
}
def get_AMI_ID(Object deploymentdata){
    
    container('devopstoolsimage') {
            dir("${workspace}") {
                muleonpremnew_artifact_pull(deploymentdata)
               sh """
                   echo "In get AMI ID"
                   ls -a
                   pwd
               """
                script{
                AMI = sh(returnStdout: true, script: "cat mule-mule-onprem-sample-api-onprem-new-manifest.json | jq -r .builds[-1].artifact_id |  cut -d':' -f2 | tr -d '\n'")
                }
               //Printing AMI ID
               // sh """
               //    $AMI 
               // """
               //return AMI;
                deploymentdata.ami_id = AMI
           }

}

}
def muleonpremnew_artifact_pull(Object deploymentdata) {
   //return {
       StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("ARTIFACTORY_ACCESS",
       StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
       def artifactoryUser = creds.getUsername()
       def artifactoryPass = Secret.toString(creds.getPassword())
       container('devopstoolsimage') {
            dir("${workspace}") {
               sh """
                    echo "Downloading the artifact to extract AMI ID"
                    wget http://${deploymentdata.artifact_server_url}/artifactory/libs-snapshot-local/${deploymentdata.jobName}/${deploymentdata.branch}/${deploymentdata.version}_${deploymentdata.git_commit_short_head}/${deploymentdata.artifactName}-manifest.json
                    ls -a
                    pwd
               """
           }
       }
}


def getSingleFileFromGit(String httpsUrl, String branch) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("GIT_ACCESS",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()
    def gitPass = Secret.toString(creds.getPassword())
    String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary("${gitUser}:${gitPass}".getBytes())
    def configUrl = "${httpsUrl}?at=${branch}&raw"
    URLConnection connection
    InputStream inputStream
    //def err

    connection = configUrl.toURL().openConnection()
    connection.setRequestProperty("Authorization", basicAuth)
    inputStream = connection.getInputStream()
/*
    try {
      inputStream = connection.getInputStream()
    } catch (e) {
      inputStream = connection.getErrorStream()
      err = e.toString()
    }
 */
    return inputStream.getText()
}

/**
 * Returns a map representing all properties (tags) for the specified Artifactory object
 * @param deploymentdata
 * @return map representing properties
 */
def getArtifactProperties(Object deploymentdata){

    deploymentdata.artifactPath = "libs-snapshot-local/${deploymentdata.jobName}/${deploymentdata.branch}/${deploymentdata.version}_${deploymentdata.git_commit_short_head}/${deploymentdata.artifactName}.zip"

    withCredentials([usernameColonPassword(credentialsId: 'ARTIFACTORY_ACCESS', variable: 'ARTIFACTORY_ACCESS')]) {
        container('devopstoolsimage') {
            response = sh (returnStdout: true, script:"""
                curl --silent -u "${ARTIFACTORY_ACCESS}" -X GET "http://${deploymentdata.artifact_server_url}/api/storage/${deploymentdata.artifactPath}?properties"
            """)
        }
    }
    println response
    parsedResponse = readJSON(text: response)
    return parsedResponse.properties

}

/**
 * Adds the passed map as properties on the specified artifact in Artifactory
 * @param deploymentdata          Artifactory path to the artifact being modified
 * @param artifactProperties    Map of property name and value pairs. Each value should be a list of values as
                                artfiactory allows passing multiple values for each property.
 */
def putArtifactProperties(Object deploymentdata, Object artifactProperties){
    deploymentdata.artifactPath = "libs-snapshot-local/${deploymentdata.jobName}/${deploymentdata.branch}/${deploymentdata.version}_${deploymentdata.git_commit_short_head}/${deploymentdata.artifactName}.zip"
    def ENC_PIPE = '%7C'        //Encoded Pipe | character
    withCredentials([usernameColonPassword(credentialsId: 'ARTIFACTORY_ACCESS', variable: 'ARTIFACTORY_ACCESS')]) {
        println "artifactProperties: ${new JsonBuilder( artifactProperties ).toPrettyString()}"
        propertiesString = ""
        artifactProperties.each {k,v ->
            propertiesString += "${k}=${java.net.URLEncoder.encode(v.join(","), "UTF-8")}" + ENC_PIPE
        }
        println ("propertiesString=${propertiesString}")
        container('devopstoolsimage') {
              //drop last semicolon
            sh (script:"""
                curl --silent -u "${ARTIFACTORY_ACCESS}" -X PUT "http://${deploymentdata.artifact_server_url}/api/storage/${deploymentdata.artifactPath}?properties=${propertiesString.substring(0,propertiesString.lastIndexOf(ENC_PIPE))}"
            """)
        }
    }
}


return this
